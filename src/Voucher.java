import java.util.*;

public class Voucher {
    public static void main(String[] args) {
        mainMenu();
    }

    private static void mainMenu() {
        System.out.println("Sebuah platform memiliki fitur loyalty program dalam bentuk poin.");
        System.out.println("Poin itu nantinya bisa di tukarkan untuk redeem sebuah voucher pulsa.");
        System.out.println("Berikut list voucher pulsa nya :");
        System.out.println("Voucher 10rb = 100p");
        System.out.println("Voucher 25rb = 200p");
        System.out.println("Voucher 50rb = 400p");
        System.out.println("Voucher 100rb = 800p");
        System.out.println("Menu fungsi program untuk kasus berikut :");
        System.out.println("1. Menentukan voucher dgn point terbesar");
        System.out.println("2. Menghitung sisa poin setelah di redeem dgn point terbesar jika poin yg dimiliki adalah 1000p");
        System.out.println("3. Redem all point dgn memprioritaskan poin terbesar. Contoh jika memiliki 1150p, maka akan mendapatkan voucher 100rb x1, 25rb x1 dan 10rb xq dgn sisa poin adalah 50p");
        Scanner input = new Scanner(System.in);
        String angka;
        System.out.print("pilih: ");
        angka = input.next();
        switch (angka) {
            case "1" -> Terbesar();
            case "2" -> SisaPoint();
            case "3" -> Redeem();
            case "0" -> {
                System.out.println("keluar dari aplikasi");
                System.exit(0);
            }
            default -> {
                System.out.println("salah memasukkan angka coba lagi\n");
                mainMenu();
            }
        }
    }

    private static void Redeem() {
        int p1 = 800, p2 = 400, p3 = 200, p4 = 100, point = 1150;
        int v1 = 100, v2 = 50, v3 = 25, v4 = 10;
        int sisa1 = point-p1, sisa2 = sisa1-p3, sisa3 = sisa2-p4;
        int vocPertama = point/p1, vocKedua = sisa1/p3, vocTiga = sisa2/p4;

        System.out.println("\nJawaban soal nomor 3");
        System.out.println("Jika point yang mau ditukar 1150p");
        System.out.println("mendapatkan voc "+v1+"rb sebanyak: "+vocPertama+"x");
        System.out.println("mendapatkan voc "+v3+"rb sebanyak: "+vocKedua+"x");
        System.out.println("mendapatkan voc "+v4+"rb sebanyak: "+vocTiga+"x");
        System.out.println("sisa point: "+sisa3+"p\n");
        System.out.println("tekan apa aja untuk kembali ke menu utama");
        Scanner balik = new Scanner(System.in);
        char sembarang = balik.next().charAt(0);
        if (sembarang >= 'a' && sembarang <= 'z'){
            mainMenu();
        } else {
            mainMenu();
        }
    }


    private static void SisaPoint() {
        int point = 1000;
        ArrayList<Integer> voc = new ArrayList<>();
        voc.add(100);
        voc.add(200);
        voc.add(400);
        voc.add(800);
        System.out.println("\nJawaban soal nomor 2");
        System.out.println("sisa point: "+(point%Collections.max(voc))+"p\n");
        System.out.println("tekan apa aja untuk kembali ke menu utama");
        Scanner balik = new Scanner(System.in);
        char sembarang = balik.next().charAt(0);
        if (sembarang >= 'a' && sembarang <= 'z'){
            mainMenu();
        } else {
            mainMenu();
        }
    }

    private static void Terbesar() {
        System.out.println("masukkan point untuk dapat nilai voc terbesar:");
        Scanner input = new Scanner(System.in);
        int p = input.nextInt();

        int a = 800, b = 400, c = 200, d = 100;
        int e = 100, f = 50, g = 25, h = 10;

        System.out.println("\nJawaban soal nomor 1");
        if (p >= a) {
            System.out.println("Voucher terbesar adalah : "+e+"rb");
        }else if(p>=b){
            System.out.println("Voucher terbesar adalah : "+f+"rb");
        }else if(p>=c){
            System.out.println("Voucher terbesar adalah : "+g+"rb");
        }else if(p>=d) {
            System.out.println("Voucher terbesar adalah : " +h+ "rb");
        }else{
            System.out.println("point kurang untuk mendapatkan voucher");
        }

        System.out.println("\ntekan apa aja untuk kembali ke menu utama");
        Scanner balik = new Scanner(System.in);
        char sembarang = balik.next().charAt(0);
        if (sembarang >= 'a' && sembarang <= 'z'){
            mainMenu();
        } else {
            mainMenu();
        }
    }
}


